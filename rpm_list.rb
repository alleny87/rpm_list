Facter.add(:rpm) do
  confine :osfamily => "RedHat"  
  setcode do
    rpm_hash={}
    rpm_list = Facter::Core::Execution.exec('rpm -qa --queryformat "%{NAME}\t%{VERSION}\n"')
    rpm_list.each_line do |row|
      value = row.chomp.split("\t")
      rpm_hash.store(value[0],value[1])
    end
    rpm_hash
  end
end
